from random import randrange # importe la fonction randrange du module random
def estADN(chaine) :
    '''
    fontion boléenne qui retourne:
    False : si un des caractères de la variable chaine
    n'est pas la lettre A,C,G ou T
    True : si la chaine ne comporte que ces caractères
    '''
    # declaration d'une variable bolléenne à Vraie
    bolleen = True
    # test pour déterminer si l'un des caractères composant la chaine
    # est différent de A,C,G ou T
    for a in chaine : # on parcourt la chaine caractère par caractère
        if not (a in "ACGT") :# on teste pour savoir si le caractère n'est pas dans la chaîne AGCT
            bolleen=False # on passe alors False à la variable
    return bolleen # on retourne le contenu de la variable



def genereADN(longueur) :
    '''
    fonction qui génère une chaine aléatoire d'ADN
    de longueur donnée
    on effectue un tirage aléatoire uniforme
    '''
    chaine="" # on définit une chaine vide
    correspondance = {# on crée un dictionnaire de correspondances
            1:"A", # à la clé 1 on attribue la valeur "A"
            2:"C",
            3:"G",
            4:"T"
            }
    for i in range(1,longueur+1) : # on crée une boucle de longueur n
        a = randrange(1,5) # on tire un entier aléatoire entre 1 et 4
        chaine=chaine+correspondance.get(a,"X") # on récupère la valeur correspondant à la clé, sinon "X3
    return chaine

def baseComplementaire(base,sequence) :
    '''
    fonction qui transcrit les éléments de la base A,C,G ou T
    en complémentaire
    respectivement T,G,C et A si la séquence est ADN
    respectivement U,G,C, et A si la sequence est ARN
    '''
    if not estADN(base) :
        return "Votre base n'est pas une base ADN"
    correspondanceADN = {# on crée un dictionnaire de correspondances compélmentaires ADN
            "A":"T", # à la clé "A" on attribue la valeur "T"
            "C":"G",
            "G":"C",
            "T":"A"
            }
    correspondanceARN = {# on crée un dictionnaire de correspondances compélmentaires ARN
            "A":"U", # à la clé "A" on attribue la valeur "T"
            "C":"G",
            "G":"C",
            "T":"A"
            }
    if sequence =="ADN" :
        return correspondanceADN.get(base,"X") # on retourne le complémentaire de la base ADN
    if sequence =="ARN" :
        return correspondanceARN.get(base,"X") # sinon le complémentaire de la base ARN
    return "Préciser la séquence"

def transcrit(chaine,debut,fin) :
    '''
    transcript et retorune une chaine ADN en ARN à partir de la position debut
    jusqu'à la position fin
    '''
    chaine_retour = ""
    for i in range(debut,fin+1) :
        chaine_retour=chaine_retour+baseComplementaire(chaine[i-1],"ARN")
    return chaine_retour

def codeGenetique(codon) :
    '''
    fonction qui traduit les codons formés de 3 bases ADN
    en acides aminés définit par lettres
    * représente les codons Stop
    '''
    corrrespondance_code_acide = { # on definit un dictionnaire des correspondances codons acides aminés
        'UUU' : 'F', 'UUC' : 'F',
        'UUA': 'L', 'UUG': 'L', 'CUU': 'L', 'CUC': 'L', 'CUA': 'L', 'CUG' : 'L',
        'AUU': 'I', 'AUC': 'I', 'AUA' : 'I',
        'AUG' : 'M',
        'GUU': 'V', 'GUC': 'V', 'GUA': 'V', 'GUG' : 'V',
        'UCU': 'S', 'UCC': 'S', 'UCA': 'S', 'UCG': 'S', 'AGU': 'S', 'AGC' : 'S',
        'CCU': 'P', 'CCC': 'P', 'CCA': 'P', 'CCG' : 'P',
        'ACU': 'T', 'ACC': 'T', 'ACA': 'T', 'ACG': 'T' ,
        'GCU': 'A', 'GCC': 'A', 'GCA': 'A', 'GCG' : 'A',
        'UAU': 'Y', 'UAC' : 'Y',
        'UAA': '*', 'UAG': '*', 'UGA' : '*',
        'CAU': 'H', 'CAC' : 'H',
        'CAA': 'Q', 'CAG' : 'Q',
        'AAU': 'N', 'AAC': 'N' ,
        'AAA': 'K', 'AAG' : 'K',
        'GAU': 'D', 'GAC' : 'D',
        'GAA': 'E', 'GAG' : 'E',
        'UGU': 'C', 'UGC' : 'C',
        'UGG' : 'W',
        'CGU': 'R', 'CGC': 'R', 'CGA': 'R', 'CGG': 'R', 'AGA': 'R', 'AGG' : 'R',
        'GGU': 'G', 'GGC': 'G', 'GGA': 'G', 'GGG' : 'G',
        }
    return corrrespondance_code_acide.get(codon,"Erreur, vous n'avez pas donné un condon de 3 bases")


def traduit(chaine) :
    '''
    fonction qui traduit une chaine ARN en chaine d'acides aminés
    '''
    chaine_aminee=""
    for i in range(0,len(chaine),3) :
                   chaine_aminee=chaine_aminee+codeGenetique(chaine[i:i+3])
    return chaine_aminee
def replique(chaine) :
    '''
    fonction qui traduit une chaine ADN en sa complémentaire inversée
    '''
    if not estADN(chaine) :
        return "Votre chaine n'est pas une chaine de bases ADN"
    chaine_complementaire="" # on définit la chaine complémentaire
    for i in range(len(chaine)-1,-1,-1) : # on parcout la chaine à l'envers
        # on traduit chaque base ADN en son complémentaire ADN en faisant
        # appel à la fonction baseComplementaire
        chaine_complementaire=chaine_complementaire+baseComplementaire(chaine[i],"ADN") 
    return chaine_complementaire

    
    
    
    
    
    